﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using Microsoft.Win32;
using System.Xml;
using System.GACManagedAccess;
using System.Collections;
using System.IO;

namespace WebpostingInstaller
{
    [RunInstaller(true)]
    public class WpInstaller : Installer
    {

        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);
        }

        protected override void OnCommitted(IDictionary savedState)
        {
            base.OnCommitted(savedState);


            string installDirectory = System.IO.Path.GetDirectoryName(Context.Parameters["assemblypath"]);
            string exePath = string.Format(@"{0}\BookmarkingBot.exe", installDirectory);

            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(exePath);
            config.ConnectionStrings.ConnectionStrings["BotEntityContext"].ConnectionString = string.Format(@"metadata=res://*/BookmarkingBot.csdl|res://*/BookmarkingBot.ssdl|res://*/BookmarkingBot.msl;provider=System.Data.SQLite;provider connection string='data source=""{0}\BookmarkingBotDB.sdb""'", installDirectory);
            config.Save();



            this.InstallSQLiteDataProvider(string.Format(@"{0}\SQLite", installDirectory));
        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            base.OnBeforeUninstall(savedState);
            this.UninstallSQLiteDataProvider();
        }

      

        private void InstallSQLiteDataProvider(string path)
        {
            #region Set AssemblyFolder in Registry
            RegistryKey sqliteKey = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\.NETFramework\\v2.0.50727\\AssemblyFoldersEx\\SQLite");

            using (RegistryKey key = Registry.LocalMachine.CreateSubKey("Software\\Microsoft\\.NETFramework\\v2.0.50727\\AssemblyFoldersEx\\SQLite", RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                key.SetValue(null, path);
            }
            #endregion

            #region Add factory support to the machine.config file.
            try
            {
                string xmlFileName = Environment.ExpandEnvironmentVariables("%WinDir%\\Microsoft.NET\\Framework\\v2.0.50727\\CONFIG\\machine.config");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(xmlFileName);


                XmlNode xmlNode = xmlDoc.SelectSingleNode("configuration/system.data/DbProviderFactories/add[@invariant=\"System.Data.SQLite\"]");
                if (xmlNode == null)
                {
                    XmlNode xmlConfig = xmlDoc.SelectSingleNode("configuration");
                    if (xmlConfig != null)
                    {
                        XmlNode xmlData = xmlConfig.SelectSingleNode("system.data");
                        if (xmlData == null)
                        {
                            xmlData = xmlDoc.CreateNode(XmlNodeType.Element, "system.data", "");
                            xmlConfig.AppendChild(xmlData);
                        }
                        XmlNode xmlParent = xmlData.SelectSingleNode("DbProviderFactories");
                        if (xmlParent == null)
                        {
                            xmlParent = xmlDoc.CreateNode(XmlNodeType.Element, "DbProviderFactories", "");
                            xmlData.AppendChild(xmlParent);
                        }

                        xmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "add", "");
                        xmlNode.Attributes.SetNamedItem(xmlDoc.CreateAttribute("name"));
                        xmlNode.Attributes.SetNamedItem(xmlDoc.CreateAttribute("invariant"));
                        xmlNode.Attributes.SetNamedItem(xmlDoc.CreateAttribute("description"));
                        xmlNode.Attributes.SetNamedItem(xmlDoc.CreateAttribute("type"));
                        xmlParent.AppendChild(xmlNode);
                    }
                }

                xmlNode.Attributes.GetNamedItem("name").Value = "SQLite Data Provider";
                xmlNode.Attributes.GetNamedItem("invariant").Value = "System.Data.SQLite";
                xmlNode.Attributes.GetNamedItem("description").Value = ".Net Framework Data Provider for SQLite";
                xmlNode.Attributes.GetNamedItem("type").Value = "System.Data.SQLite.SQLiteFactory, System.Data.SQLite, Version=1.0.65.0, Culture=neutral, PublicKeyToken=db937bc2d44ff139";

                xmlDoc.Save(xmlFileName);
            }
            catch
            {
            }
            #endregion

            #region Add SQLite Assemblies to Global Assembly Cache
            string SQLiteLocation = string.Format(@"{0}\System.Data.SQLite.DLL", path);
            try
            {
                AssemblyCache.InstallAssembly(SQLiteLocation, null, AssemblyCommitFlags.Default);
                AssemblyCache.InstallAssembly(Path.Combine(Path.GetDirectoryName(SQLiteLocation), "System.Data.SQLite.Linq.DLL"), null, AssemblyCommitFlags.Default);
            }
            catch
            {

            }
            #endregion

        }

        private void UninstallSQLiteDataProvider()
        {
            #region Remove AssemblyFolder in Registry
            RegistryKey sqliteKey = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\.NETFramework\\v2.0.50727\\AssemblyFoldersEx\\SQLite");

            if (sqliteKey != null)
            {
                sqliteKey.Close();
                Registry.LocalMachine.DeleteSubKey("Software\\Microsoft\\.NETFramework\\v2.0.50727\\AssemblyFoldersEx\\SQLite");
            }
            #endregion

            #region  Remove factory support to the machine.config file.
            string xmlFileName = Environment.ExpandEnvironmentVariables("%WinDir%\\Microsoft.NET\\Framework\\v2.0.50727\\CONFIG\\machine.config");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Load(xmlFileName);

            XmlNode xmlNode = xmlDoc.SelectSingleNode("configuration/system.data/DbProviderFactories/add[@invariant=\"System.Data.SQLite\"]");

            if (xmlNode != null)
                xmlNode.ParentNode.RemoveChild(xmlNode);

            xmlNode = xmlDoc.SelectSingleNode("configuration/system.data/DbProviderFactories/remove[@invariant=\"System.Data.SQLite\"]");
            if (xmlNode != null)
                xmlNode.ParentNode.RemoveChild(xmlNode);

            xmlDoc.Save(xmlFileName);
            #endregion

            #region Remove SQLite Assemblies to Global Assembly Cache
            try
            {
                AssemblyCacheUninstallDisposition disp;
                AssemblyCacheEnum entries = new AssemblyCacheEnum("System.Data.SQLite");
                string s;

                while (true)
                {
                    s = entries.GetNextAssembly();
                    if (String.IsNullOrEmpty(s)) break;
                    AssemblyCache.UninstallAssembly(s, null, out disp);
                }

                entries = new AssemblyCacheEnum("System.Data.SQLite.Linq");

                while (true)
                {
                    s = entries.GetNextAssembly();
                    if (String.IsNullOrEmpty(s)) break;
                    AssemblyCache.UninstallAssembly(s, null, out disp);
                }

            }
            catch
            {

            }
            #endregion
        }

        private void InitializeComponent()
        {
            // 
            // WpInstaller
            // 
           

        }

       
    }
}
